## Drone API

### Getting Started

**Prerequisite**
- Java 17
- Maven 4

**To build, Run and Test**
- Run the following command in a terminal window (in the complete directory):
```
mvnw spring-boot:run
```
At present this app can only be run locally and is hosted at the default, `http://127.0.0.1:8080/`

### Error Handling
Errors are returned as JSON objects in the following format:
```
{
    "success": false,
    "message": "Drone Passed weight limit (500 max)",
    "error": "unable to process",
    "status": 422
}
```
The API will return these error types when requests fail:
- 404: Resource Not Found
- 405: Method Not Allowed
- 422: Not Processable 

### Endpoints 
##### POST /drone
- Add a new Drone.
- Returns a success value, message and status. 
- `curl http://127.0.0.1:8080/drone -X POST -H "Content-Type: application/json" -d '{"serialNumber":"DY500011", "model":"Middleweight", "weightLimit":"305", "batteryCapacity":"64", "state":"IDLE"}'`
```
{
    "success": true,
    "message": "Drone was successfully added",
    "status": 200
}
```

##### POST /medication
- Add a new Medication.
- Returns a success value, message and status. 
- `curl http://127.0.0.1:8080/medication -X POST -H "Content-Type: application/json" -d '{"name":"med-05", "weight":120, "code":"CD_MED_705", "image":"medication_image"}'`
```
{
    "success": true,
    "message": "Medication was successfully added",
    "status": 200
}
```

##### PATCH /drone/{serialNumber}/medication/{code}
- Load a drone with medication.
- Returns a success value, message and status. 
- `curl http://127.0.0.1:8080/drone/{serialNumber}/medication/{code} -X PATCH -H "Content-Type: application/json"`
```
{
    "success": true,
    "message": "Drone was successfully loaded",
    "status": 200
}
```

##### GET /medication/drone/{serialNumber}
- Get loaded medication
- Returns a success value, status and loaded medication
- `curl http://127.0.0.1:8080/medication/drone/{serialNumber}`

``` 
{
    "success": true,
    "medication": {
        "medId": 14,
        "name": "med-04",
        "weight": 50.0,
        "code": "CD_MED_605",
        "image": "medimage04",
        "droneId": 10
    },
    "status": 200
}
```

##### GET /drones/available
- Get available drones
- Returns a success value, status and available drones
- `curl http://127.0.0.1:8080/drones/available`

``` 
{
    "drones": [
        {
            "id": 1,
            "serialNumber": "DY500001",
            "model": "Lightweight",
            "weightLimit": 155.0,
            "batteryCapacity": 30,
            "state": "IDLE"
        },
        {
            "id": 7,
            "serialNumber": "DY500007",
            "model": "Cruiserweight",
            "weightLimit": 350.0,
            "batteryCapacity": 70,
            "state": "IDLE"
        }
    ],
    "success": true,
    "status": 200
}
```

##### GET /drone/{serialNumber}/batterylevel
- Check a drone battery level
- Returns a success value, status and drone battery level
- `curl http://127.0.0.1:8080/drone/{serialNumber}/batterylevel`
``` 
{
    "success": true,
    "batterylevel": "60%",
    "status": 200
}
```

