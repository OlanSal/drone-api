package com.drone.api;

import java.util.TimerTask;
 
import java.util.logging.Logger; 
import org.springframework.beans.factory.annotation.Autowired;

public class PeriodicTask extends TimerTask {
	
	private DroneRepository droneRepo;
	
	PeriodicTask(DroneRepository droneRepo){
		this.droneRepo = droneRepo;
	}

	@Override
	public void run() {
		Logger logger = Logger.getLogger(PeriodicTask.class.getName());
		logger.info("****************START****************");
		for(DroneModel droneModel : droneRepo.findAll()) {
			logger.info("Battery level for drone serial number ("+droneModel.getSerialNumber()+") is "+droneModel.getBatteryCapacity()+"%");
		}
		logger.info("***************END*****************");
	}

}
