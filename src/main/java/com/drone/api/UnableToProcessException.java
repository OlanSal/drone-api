package com.drone.api;

public class UnableToProcessException extends RuntimeException  {
	UnableToProcessException(String message){
		super(message);
	}
}
