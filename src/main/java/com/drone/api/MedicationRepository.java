package com.drone.api;

import org.springframework.data.jpa.repository.JpaRepository;

interface MedicationRepository extends JpaRepository<MedicationModel, Long> {
	
	MedicationModel findByCode(String code);
	
	MedicationModel findByDroneId(Long droneId);
}
