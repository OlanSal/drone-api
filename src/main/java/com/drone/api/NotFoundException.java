package com.drone.api;

public class NotFoundException extends RuntimeException  {
	NotFoundException(String message){
		super(message);
	}
}
