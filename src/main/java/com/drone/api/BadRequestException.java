package com.drone.api;

public class BadRequestException extends RuntimeException  {
	
	BadRequestException(String message){
		super(message);
	}
	
}
