package com.drone.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class DroneExceptionAdvice {
	
	@ResponseBody
	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String notFoundHandler(NotFoundException ex) {
		return ex.getMessage();
	}
	
	@ResponseBody
	@ExceptionHandler(UnableToProcessException.class)
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	String unableToProcessHandler(UnableToProcessException ex) {
		return ex.getMessage();
	}
	
	@ResponseBody
	@ExceptionHandler(BadRequestException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	String notFoundHandler(BadRequestException ex) {
		return ex.getMessage();
	}
	
	@ResponseBody
	@ExceptionHandler(MethodNotAllowedException.class)
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	String notFoundHandler(MethodNotAllowedException ex) {
		return ex.getMessage();
	}
	

}
