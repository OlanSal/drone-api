package com.drone.api;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

interface DroneRepository extends JpaRepository<DroneModel, Long> {
	
	DroneModel findBySerialNumber(String serialNumber);
	
	List<DroneModel> findByState(String state);

}
