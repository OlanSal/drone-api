package com.drone.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins="*")
class DroneController {
	
	enum State {
		IDLE,
		LOADING,
		LOADED,
		DELIVERING,
		DELIVERED,
		RETURNING
	}
	
	private static final double MAX_WEIGHT_LIMIT = 500.0;
	
	private String errUnableToProcess = "unable to process";
	private String errNotFound = "resource not found";
	private String errKey = "error";
	private String successKey = "success";
	private String statusKey = "status";
	private String msgKey = "message";
	
	private Map<String, Object> map;
	private DroneRepository droneRepo;
	private MedicationRepository medRepo;
	
	DroneController(DroneRepository droneRepo, MedicationRepository medRepo){
		this.droneRepo = droneRepo;
		this.medRepo = medRepo;
	}
	
	private ResponseEntity<Map<String, Object>> rxp(HttpStatus status, String error){
		if(error != null) {
			map.put(errKey, error);
		}
		map.put(statusKey, status.value());
		
		HttpHeaders h = new HttpHeaders();
		h.setContentType(MediaType.APPLICATION_JSON);
		return new ResponseEntity<Map<String, Object>>(
				map, 
				h, 
				status);
		
	}
	
	@PostMapping("/drone")
	public ResponseEntity<Map<String, Object>> registerDrone(@RequestBody DroneModel droneModel) {
		map = new HashMap<>();
		map.put(successKey, false);
		
		if(droneModel.getSerialNumber().isEmpty()) {
			map.put(msgKey, "Serial Number is empty");
			return rxp(HttpStatus.UNPROCESSABLE_ENTITY, errUnableToProcess);
		}
			
		
		if(droneRepo.findBySerialNumber(droneModel.getSerialNumber()) != null) {
			map.put(msgKey, "Drone already exist");
			return rxp(HttpStatus.UNPROCESSABLE_ENTITY, errUnableToProcess);
		}
		
		if(droneModel.getSerialNumber().length() > 100){
			map.put(msgKey, "Serial Number too long (100 characaters max)");
			return rxp(HttpStatus.UNPROCESSABLE_ENTITY, errUnableToProcess);
		}
		
		if(droneModel.getWeightLimit() > MAX_WEIGHT_LIMIT) {
			map.put(msgKey, "Drone Passed weight limit (500 max)");
			return rxp(HttpStatus.UNPROCESSABLE_ENTITY, errUnableToProcess);
		}
		
		droneRepo.save(droneModel);
		
		map.put(successKey, true);
		map.put(msgKey, "Drone was successfully added");
		return rxp(HttpStatus.OK, null);
		
	}
	
	@PostMapping("/medication")
	public ResponseEntity<Map<String, Object>> registerMedication(@RequestBody MedicationModel medicationModel) {
		map = new HashMap<>();
		map.put(successKey, false);
		
		if(medicationModel.getName().isEmpty()) {
			map.put(msgKey, "Medication Name is empty");
			return rxp(HttpStatus.UNPROCESSABLE_ENTITY, errUnableToProcess);
		}
			
		
		if(!medicationModel.getName().matches("^[0-9A-Za-z_-]+$")) {
			map.put(msgKey, "Name must contain only letters, numbers, _ and -");
			return rxp(HttpStatus.UNPROCESSABLE_ENTITY, errUnableToProcess);
			
		}
		
		if(medicationModel.getCode().isEmpty()) {
			map.put(msgKey, "Medication Code is empty");
			return rxp(HttpStatus.UNPROCESSABLE_ENTITY, errUnableToProcess);
		}
		
		if(!medicationModel.getCode().matches("^[0-9A-Z_]+$")) {
			map.put(msgKey, "Code must contain only upper case letters, numbers and _");
			return rxp(HttpStatus.UNPROCESSABLE_ENTITY, errUnableToProcess);
		}
		
		if(medRepo.findByCode(medicationModel.getCode()) != null) {
			map.put(msgKey, "Medication already exist");
			return rxp(HttpStatus.UNPROCESSABLE_ENTITY, errUnableToProcess);
		}
		
		medRepo.save(medicationModel);
		
		map.put(successKey, true);
		map.put(msgKey, "Medication was successfully added");
		return rxp(HttpStatus.OK, null);
		
	}
	
	@PatchMapping("/drone/{serialNumber}/medication/{code}")
	public ResponseEntity<Map<String, Object>> loadDrone(@PathVariable String serialNumber, @PathVariable String code) {
		map = new HashMap<>();
		map.put(successKey, false);
		
		DroneModel droneModel = droneRepo.findBySerialNumber(serialNumber);
		if(droneModel == null) {
			map.put(msgKey, "Drone with Serial Number "+serialNumber+" not found");
			return rxp(HttpStatus.NOT_FOUND, errNotFound);
		}
		
		MedicationModel medicationModel = medRepo.findByCode(code);
		if(medicationModel == null) {
			map.put(msgKey, "Medication with Code "+code+" not found");
			return rxp(HttpStatus.NOT_FOUND, errNotFound);
		}
		
		if(droneModel.getBatteryCapacity() < 25) {
			map.put(msgKey, "Unable to load drone. Battery level is below 25%");
			return rxp(HttpStatus.UNPROCESSABLE_ENTITY, errUnableToProcess);
		}
		
		if(medicationModel.getWeight() > droneModel.getWeightLimit()) {
			map.put(msgKey, "Medication weight passed drone weight limit");
			return rxp(HttpStatus.UNPROCESSABLE_ENTITY, errUnableToProcess);
		}
		
		medicationModel.setDroneId(droneModel.getId());
		medRepo.save(medicationModel);
		
		droneModel.setState(State.LOADING.name());
		droneRepo.save(droneModel);
		
		//return success
		map.put(successKey, true);
		map.put(msgKey, "Drone was successfully loaded");
		return rxp(HttpStatus.OK, null);
		
		
	}
	
	@GetMapping("/medication/drone/{serialNumber}")
	public ResponseEntity<Map<String, Object>> loadedMedication(@PathVariable String serialNumber) {
		map = new HashMap<>();
		map.put(successKey, false);
		
		DroneModel droneModel = droneRepo.findBySerialNumber(serialNumber);
		if(droneModel == null) {
			map.put(msgKey, "Drone with Serial Number "+serialNumber+" not found");
			return rxp(HttpStatus.NOT_FOUND, errNotFound);
		}
		
		MedicationModel medicationModel = medRepo.findByDroneId(droneModel.getId());
		if(medicationModel == null) {
			map.put(msgKey, "No Medication found");
			return rxp(HttpStatus.NOT_FOUND, errNotFound);
		}
		
		map.put(successKey, true);
		map.put("medication", medicationModel);
		return rxp(HttpStatus.OK, null);
		
		
	}
	
	@GetMapping("/drones/available")
	public ResponseEntity<Map<String, Object>> availableDrones() {
		List<DroneModel> droneList = droneRepo.findByState(State.IDLE.name());
		
		map = new HashMap<>();
		map.put(successKey, true);
		map.put("drones", droneList);
		return rxp(HttpStatus.OK, null);
	}
	
	@GetMapping("/drone/{serialNumber}/batterylevel")
	public ResponseEntity<Map<String, Object>> batteryLevel(@PathVariable String serialNumber) {
		map = new HashMap<>();
		
		DroneModel droneModel = droneRepo.findBySerialNumber(serialNumber);
		if(droneModel == null) {
			map.put(successKey, false);
			map.put(msgKey, "Drone with Serial Number "+serialNumber+" not found");
			return rxp(HttpStatus.NOT_FOUND, errNotFound);
		}
		
		map.put(successKey, true);
		map.put("batterylevel", droneModel.getBatteryCapacity()+"%");
		return rxp(HttpStatus.OK, null);
		
	}
	
	@GetMapping("/")
	public ResponseEntity<Map<String, Object>> mainX() {
		map = new HashMap<>();
		map.put(successKey, true);
		map.put(msgKey, "Drone API");
		return rxp(HttpStatus.OK, null);
	}

}
