package com.drone.api;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
class MedicationModel {
	
	@Id
	@GeneratedValue
	private Long medId;
	private String name;
	private double weight;
	private String code;
	private String image;
	private Long droneId;
	
	MedicationModel(){
		
	}
	
	MedicationModel(String name, double weight, String code, String image){
		this.name = name;
		this.weight = weight;
		this.code = code;
		this.image = image;
	}

	public Long getMedId() {
		return medId;
	}

	public void setMedId(Long medId) {
		this.medId = medId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getDroneId() {
		return droneId;
	}

	public void setDroneId(Long droneId) {
		this.droneId = droneId;
	}
	

}
