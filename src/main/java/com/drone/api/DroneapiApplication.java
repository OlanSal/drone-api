package com.drone.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

@SpringBootApplication
public class DroneapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DroneapiApplication.class, args);
		
	}
	
	@Order(Ordered.HIGHEST_PRECEDENCE)
	@Bean
	public CommandLineRunner loadDrone(DroneRepository droneRepo) {
		return arg -> {
			droneRepo.save(new DroneModel("DY500001", "Lightweight", 155, 30, "IDLE"));
			droneRepo.save(new DroneModel("DY500002", "Middleweight", 255, 40, "LOADING"));
			droneRepo.save(new DroneModel("DY500003", "Cruiserweight", 355, 50, "LOADED"));
			droneRepo.save(new DroneModel("DY500004", "Heavyweight", 485, 70, "DELIVERING"));
			droneRepo.save(new DroneModel("DY500005", "Lightweight", 155, 60, "DELIVERED"));
			droneRepo.save(new DroneModel("DY500006", "Middleweight", 155, 10, "RETURNING"));
			droneRepo.save(new DroneModel("DY500007", "Cruiserweight", 350, 70, "IDLE"));
			droneRepo.save(new DroneModel("DY500008", "Middleweight", 235, 90, "LOADING"));
			droneRepo.save(new DroneModel("DY500009", "Heavyweight", 500, 20, "LOADED"));
			droneRepo.save(new DroneModel("DY500010", "Lightweight", 135, 40, "LOADED"));
		};
		
	}
	
	@Order(Ordered.LOWEST_PRECEDENCE)
	@Bean
	public CommandLineRunner loadMed(MedicationRepository medRepo) {
		return arg -> {
			List<MedicationModel>  medList = new ArrayList<>();
			medList.add(new MedicationModel("med-01", 20, "CD_MED_305", "medimage01"));
			medList.add(new MedicationModel("med-02", 30, "CD_MED_405", "medimage02"));
			medList.add(new MedicationModel("med-03", 40, "CD_MED_505", "medimage03"));
			medList.add(new MedicationModel("med-04", 50, "CD_MED_605", "medimage04"));
			medRepo.saveAll(medList);
		};
		
	}
	
	@Order(Ordered.LOWEST_PRECEDENCE)
	@Bean
	public CommandLineRunner startPeriodicTask(DroneRepository droneRepo) {
		return arg -> {
			Timer timer = new Timer();
			timer.schedule(new PeriodicTask(droneRepo), 1000, 2000);
		};
		
	}

}
