package com.drone.api;

public class MethodNotAllowedException extends RuntimeException {
	
	MethodNotAllowedException(String message){
		super(message);
	}

}
